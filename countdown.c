///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// File: countdown.c
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Jonah Bobilin <jbobilin@hawaii.edu>
// @date   2 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char* argv[]) {

   //create valentine's day date for this year
   struct tm val_day = {5, 9, 2, 14, 5, 110};
   time_t time_ref = mktime(&val_day);

   //print label
   char label[50];
   strftime(label, 50,"%a %b %d %r %Z %Y", &val_day);
   printf("Reference time:  %s\n\n", label); 

   while(1)
   {
   //declare for time struct
   time_t current_time = time(NULL);
   
   //countdown calculations
   long int diff = difftime(current_time, time_ref);
   int year = diff / 31556952;
      diff = diff - year * 31556952;
   int day = diff / 86400;
      diff = diff - day * 86400;
   int hour = diff / 3600;
      diff = diff - hour * 3600;
   int min = diff / 60;
      diff = diff - min * 60;
   int sec = diff;   

   //print line
   printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", year, day, hour, min, sec);

   //wait 1 second
   sleep(1);
   }
   return 0;
}
